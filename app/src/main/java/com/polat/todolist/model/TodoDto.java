package com.polat.todolist.model;

import com.google.gson.annotations.SerializedName;

public class TodoDto {

    @SerializedName("title")
    public String title;

    @SerializedName("completed")
    public boolean completed;

}
