package com.polat.todolist;

import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.polat.todolist.adapter.RecyclerViewAdapter;
import com.polat.todolist.databinding.ActivityMainBinding;
import com.polat.todolist.model.TodoDto;
import com.polat.todolist.service.TodoService;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class MainActivity extends AppCompatActivity {

    ArrayList<TodoDto> list;
    private String BASE_URL = "https://jsonplaceholder.typicode.com/";
    Retrofit retrofit;

    ActivityMainBinding binding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        binding = ActivityMainBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());


        Gson gson = new GsonBuilder().setLenient().create();

        retrofit = new Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .build();

        loadData();

    }

    private void loadData(){

        final TodoService todoService = retrofit.create(TodoService.class);

       Call<List<TodoDto>> call = todoService.getData();

        call.enqueue(new Callback<List<TodoDto>>() {
            @Override
            public void onResponse(Call<List<TodoDto>> call, Response<List<TodoDto>> response) {
                if (response.isSuccessful()) {
                    List<TodoDto> responseList = response.body();
                    handleResponse(responseList);
                }
            }

            @Override
            public void onFailure(Call<List<TodoDto>> call, Throwable t) {
                t.printStackTrace();
            }
        });

    }


    private void handleResponse(List<TodoDto> todoDtoList) {
        list = new ArrayList<>(todoDtoList);

        binding.recyclerView.setAdapter(new RecyclerViewAdapter(list));
    }

}