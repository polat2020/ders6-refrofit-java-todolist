package com.polat.todolist.adapter;

import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.polat.todolist.databinding.RowLayoutBinding;
import com.polat.todolist.model.TodoDto;

import java.util.ArrayList;

public class RecyclerViewAdapter extends RecyclerView.Adapter<RowHolder> {
    private ArrayList<TodoDto> list;
    public RecyclerViewAdapter(ArrayList<TodoDto> list) {
        this.list = list;
    }

    @NonNull
    @Override
    public RowHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        RowLayoutBinding binding =RowLayoutBinding.inflate(LayoutInflater.from(parent.getContext()),parent,false);
        return new RowHolder(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull RowHolder holder, int position) {
        TodoDto todoDto = list.get(position);
        LinearLayout itemView= holder.binding.getRoot();
        if (todoDto.completed) {
            itemView.setBackgroundColor(Color.parseColor("#BAFFB4"));
        } else  {
            itemView.setBackgroundColor(Color.parseColor("#FFAB76"));
        }
        TextView txtTitle =  holder.binding.txtTitle;
        txtTitle.setText(todoDto.title);
    }

    @Override
    public int getItemCount() {
        return list.size();
    }
}
